//
//  ZIAppDelegate.h
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;

@end
