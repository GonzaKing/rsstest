//
//  ZIRSSFeed.m
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import "ZIRSSFeed.h"

@implementation ZIRSSFeed
//initialize from dictionary
- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.title = [dict objectForKey:@"title"];
        self.link = [dict objectForKey:@"link"];
        self.feedDescription = [dict objectForKey:@"description"];
        self.image = [dict objectForKey:@"image"];
        self.date = [dict objectForKey:@"date"];
    }
    return self;
}
//initialize with title link description
- (id)initWithTitle:(NSString *)title link:(NSString *)link description:(NSString *)description
{
    self = [super init];
    if (self) {
        self.title = title;
        self.feedDescription = description;
        self.link = link;
    }
    return self;
}
//converts to dictionary
- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    [dictionary setObject:self.title forKey:@"title"];
    [dictionary setObject:self.link forKey:@"link"];
    [dictionary setObject:self.feedDescription forKey:@"description"];
    [dictionary setObject:self.image ? self.image : @"" forKey:@"image"];
    [dictionary setObject:self.date forKey:@"date"];
    return dictionary;
}

- (NSString  *)description
{
    return [NSString stringWithFormat:@"Title: %@, Link: %@, Date: %@", self.title, self.link, self.date];
}
//checking news
- (BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    } else {
        if (![object isKindOfClass:[ZIRSSFeed class]]) {
            return NO;
        } else {
            ZIRSSFeed *another = object;
            if ([self.title isEqualToString:another.title]) {
                if ([self.date isEqualToString:another.date]) {
                    return YES;
                } else {
                    return NO;
                }
            } else {
                return NO;
            }
        }
    }
}
@end
