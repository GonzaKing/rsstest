//
//  ZIDownloadHelper.m
//  RSSReader
//
//  Created by admin on 29.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import "ZIDownloadHelper.h"

#import <TBXML.h>

#import "ZIRSSFeed.h"

#define RSS_URL @"http://www.sciencedaily.com/newsfeed.xml"

@interface ZIDownloadHelper()

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *loadedData;
@end

@implementation ZIDownloadHelper

- (void)loadData
{
    NSURL *url = [NSURL URLWithString:RSS_URL];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [self.connection start];
}

#pragma mark NSURLConnection delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.loadedData = [NSMutableData new];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.loadedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(downloadHelperFailedWithError:)]) {
        [self.delegate downloadHelperFailedWithError:error];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error = nil;
    TBXML *xmlParser = [[TBXML alloc] initWithXMLData:self.loadedData error:&error];
    if (error) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(downloadHelperFailedWithError:)]) {
            [self.delegate downloadHelperFailedWithError:error];
        }
    } else {
        TBXMLElement *root = xmlParser.rootXMLElement;
        TBXMLElement *channel = [TBXML childElementNamed:@"channel" parentElement:root];
        NSMutableArray *rssNews = [NSMutableArray array];
        if (channel) {
            
            TBXMLElement *item = [TBXML childElementNamed:@"item" parentElement:channel];
            while (item) {
                TBXMLElement *title = [TBXML childElementNamed:@"title" parentElement:item];
                TBXMLElement *link = [TBXML childElementNamed:@"link" parentElement:item];
                TBXMLElement *description = [TBXML childElementNamed:@"description" parentElement:item];
                TBXMLElement *image = [TBXML childElementNamed:@"image" parentElement:item];
                TBXMLElement *date = [TBXML childElementNamed:@"pubDate" parentElement:item];
                
                ZIRSSFeed *feed = [[ZIRSSFeed alloc] initWithTitle:[TBXML textForElement:title]
                                                            link:[TBXML textForElement:link]
                                                       description:[TBXML textForElement:description]];
                
                feed.date = [TBXML textForElement:date];
                if (image != NULL) {
                    feed.image = [TBXML textForElement:image];
                }
                
                [rssNews addObject:feed];
                
                
                item = [TBXML nextSiblingNamed:@"item" searchFromElement:item];
            }
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(downloadHelperFinished:)]) {
            [self.delegate downloadHelperFinished:rssNews];
        }
    }
}
@end