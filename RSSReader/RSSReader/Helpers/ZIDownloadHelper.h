//
//  ZIDownloadHelper.h
//  RSSReader
//
//  Created by admin on 29.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ZIDownloadHelperDelegate;

@interface ZIDownloadHelper : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>

@property (nonatomic, weak) id<ZIDownloadHelperDelegate> delegate;

- (void)loadData;
@end

@protocol ZIDownloadHelperDelegate <NSObject>

- (void)downloadHelperFailedWithError:(NSError *)error;
- (void)downloadHelperFinished:(id)data;
@end