//
//  ZIRSSFeed.h
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZIRSSFeed : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *feedDescription;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *date;

- (id)initWithTitle:(NSString *)title link:(NSString *)link description:(NSString *)description;
- (id)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;
@end
