//
//  ZINewsListViewController.m
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import "ZINewsListViewController.h"

#import <MBProgressHUD.h>

#import "ZIFeedViewController.h"

@interface ZINewsListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *newsTable;

@property (nonatomic, strong) NSArray *feedsList;

@property (nonatomic, strong) ZIDownloadHelper *helper;
@end

@implementation ZINewsListViewController

@synthesize helper = _helper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"News List";
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"News List" image:[UIImage imageNamed:@"logo_bear"] tag:0];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //right item allow refresh feeds
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"refresh"] style:UIBarButtonItemStylePlain target:self action:@selector(refreshAction)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//refresh content
- (void)refreshAction
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.helper loadData];
}

#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.feedsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"NewsList";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    ZIRSSFeed *feed = self.feedsList[indexPath.row];
    
    cell.textLabel.text = feed.title;
    cell.detailTextLabel.text = feed.date;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
//when row was selected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZIFeedViewController *fvc = [[ZIFeedViewController alloc] init];
    fvc.feed = self.feedsList[indexPath.row];
    [self.navigationController pushViewController:fvc animated:YES];
}

- (ZIDownloadHelper *)helper
{
    if (!_helper) {
        _helper = [[ZIDownloadHelper alloc] init];
        _helper.delegate = self;
    }
    return _helper;
}

#pragma mark Download helper section
//load from  internet
- (void)downloadHelperFailedWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [av show];
}
- (void)downloadHelperFinished:(id)data
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    self.feedsList = data;
    [self.newsTable reloadData];
    
}
@end
