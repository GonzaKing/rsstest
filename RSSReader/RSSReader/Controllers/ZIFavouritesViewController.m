//
//  ZIFavouritesViewController.m
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import "ZIFavouritesViewController.h"

#import "ZIRSSFeed.h"
#import "ZIFavouritesStore.h"
#import "ZIFeedViewController.h"

@interface ZIFavouritesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *favouritesTable;
@property (nonatomic, strong) NSArray *favourites;
@end

@implementation ZIFavouritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Favourites";
        self.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:1];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
//when view will appear
- (void)viewWillAppear:(BOOL)animated
{
    [self loadFavoiurites];
}

- (void)loadFavoiurites
{
    self.favourites = [ZIFavouritesStore allFavourites];
    [self.favouritesTable reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.favourites count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"FavouritesList";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    ZIRSSFeed *feed = self.favourites[indexPath.row];
    
    cell.textLabel.text = feed.title;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZIFeedViewController *fvc = [[ZIFeedViewController alloc] init];
    fvc.feed = self.favourites[indexPath.row];
    [self.navigationController pushViewController:fvc animated:YES];
}
@end
