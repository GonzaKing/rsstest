//
//  ZIFavouritesViewController.h
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZIFavouritesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
