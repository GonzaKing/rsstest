//
//  ZIFeedViewController.m
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import "ZIFeedViewController.h"

#import "ZIFavouritesStore.h"

@interface ZIFeedViewController ()
//displays content
@property (strong, nonatomic) IBOutlet UITextView *contentView;

@end

@implementation ZIFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.contentView.text = self.feed.feedDescription;
    self.title = self.feed.title;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self refreshIcon];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//adds right item to nsvigation bar with unselected star
- (void)refreshIcon
{
    UIImage *star;
    if (![ZIFavouritesStore isFavourite:self.feed]){
        star = [UIImage imageNamed:@"star"];
    } else {
        star = [UIImage imageNamed:@"star_active"];
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:star style:UIBarButtonItemStylePlain target:self action:@selector(addToFavourite)];
}

- (IBAction)shareTwitter:(id)sender
{
    SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetSheet setInitialText:self.feed.title];
    [tweetSheet addURL:[NSURL URLWithString:self.feed.link]];
    [self presentViewController:tweetSheet animated:YES completion:nil];
}

//Adds to favourite section
- (void)addToFavourite
{
    if(![ZIFavouritesStore isFavourite:self.feed]){
        [ZIFavouritesStore addToFavourites:self.feed];
    } else {
        [ZIFavouritesStore removeFromFavourites:self.feed];
    }
    [self refreshIcon];
}
@end
