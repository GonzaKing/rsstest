//
//  ZIFeedViewController.h
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Social/Social.h>
#import "ZIRSSFeed.h"

@interface ZIFeedViewController : UIViewController

@property (nonatomic, strong) ZIRSSFeed *feed;

@end
