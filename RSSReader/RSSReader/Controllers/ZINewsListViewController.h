//
//  ZINewsListViewController.h
//  RSSReader
//
//  Created by admin on 28.11.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZIRSSFeed.h"
#import "ZIDownloadHelper.h"

@interface ZINewsListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, ZIDownloadHelperDelegate>

@end
