//
//  ZIFavouritesStore.h
//  RSSReader
//
//  Created by admin on 01.12.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZIRSSFeed.h"

@interface ZIFavouritesStore : NSObject


+ (id)sharedInstance;
+ (BOOL)isFavourite:(ZIRSSFeed *)feed;
+ (NSArray *)allFavourites;

+ (void)addToFavourites:(ZIRSSFeed *)feed;
+ (void)removeFromFavourites:(ZIRSSFeed *)feed;
@end
