//
//  ZIFavouritesStore.m
//  RSSReader
//
//  Created by admin on 01.12.13.
//  Copyright (c) 2013 Igoreha. All rights reserved.
//

#import "ZIFavouritesStore.h"

@interface ZIFavouritesStore()

@property (nonatomic, strong) NSMutableSet *favourites;
@end

__strong static ZIFavouritesStore *store;

@implementation ZIFavouritesStore

@synthesize favourites = _favourites;

+ (id)sharedInstance
{
    if (!store) {
        store = [[ZIFavouritesStore alloc] init];
    }
    return store;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *store = [defaults objectForKey:kFavourites];
        _favourites = [NSMutableSet new];
        if (store) {
            for (NSDictionary *data in store) {
                ZIRSSFeed *feed = [[ZIRSSFeed alloc] initWithDictionary:data];
                [_favourites addObject:feed];
            }
        }
    }
    return self;
}

+ (BOOL)isFavourite:(ZIRSSFeed *)feed
{
    ZIFavouritesStore *store = [ZIFavouritesStore sharedInstance];
    return [store.favourites containsObject:feed];
}

+ (NSArray *)allFavourites
{
    ZIFavouritesStore *store = [ZIFavouritesStore sharedInstance];
    return [store.favourites allObjects];
}

+ (void)addToFavourites:(ZIRSSFeed *)feed
{
    ZIFavouritesStore *store = [ZIFavouritesStore sharedInstance];
    [store addFavourite:feed];
}

- (void)addFavourite:(ZIRSSFeed *)feed
{
    if (![self.favourites containsObject:feed]) {
        [self.favourites addObject:feed];
    }
    [self synchronize];
}

+ (void)removeFromFavourites:(ZIRSSFeed *)feed
{
    ZIFavouritesStore *store = [ZIFavouritesStore sharedInstance];
    [store removeFavourite:feed];
}

- (void)removeFavourite:(ZIRSSFeed *)feed
{
    [self.favourites removeObject:feed];
    [self synchronize];
}

- (void)synchronize
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (ZIRSSFeed *feed in self.favourites) {
        NSDictionary *store = [feed dictionaryRepresentation];
        [array addObject:store];
    }
    
    [defaults setObject:array forKey:kFavourites];
    [defaults synchronize];
}
@end
